echo This is a automated installer for osu, made for ArcoLinux and other arch based distros
echo First, we will install wine dependencies and lutris
echo You will be required to enter your password as it is gonna use sudo
echo You have 10 seconds to Ctrl+C and abort the script
echo Make sure the multilib repo is enabled!!! 
sleep 10
sudo pacman -Syyu
sudo pacman -S wine winetricks
sudo pacman -S giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox
sudo pacman -S lutris
lutris -i osu-windows.yml
echo Now follow along with the installer
echo Then, you need to run osuinstaller2.sh to install pipewire
echo Make sure to read osuinstaller2.warnings.readme
sleep 5
fi