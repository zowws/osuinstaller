# osu installer
Simple script i made for myself to install osu! on Arch

(The osuinstaller2 script installs pipewire for lower latency)

!!! Enable multilb repo !!!
# Credits
lutris for osu-windows.yml
# Installation
```
git clone https://gitlab.com/zowws/osuinstaller.git
cd osuinstaller
sh osuinstaller.sh
```
Then, in order to install pipewire execute the osuinstaller2 script
```
sh osuinstaller2.sh
```
